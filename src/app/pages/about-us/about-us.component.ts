import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {
  // Variables globales
  public imgEjemplo = '../../../assets/image/montanas.jpg';

  constructor() {
  }

  ngOnInit(): void {
  }

}
