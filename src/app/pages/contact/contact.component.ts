/* Importaciones de Angular */
import { Component, OnInit } from '@angular/core';

/* Impportaciones de formulario */
import { Validators, FormBuilder, FormGroup, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  /* Form */
  public contactForm: FormGroup;
  public name: AbstractControl;

  /* Results */
  public nameInput: string = '';

  /* Flags */
  public validName = false;
  public validInput = true;

  constructor( private formBuilder: FormBuilder ) {

    // Inicializamos el controlador de nuestro formulario
    this.contactForm = this.formBuilder.group({
      name: ['', Validators.compose( [ Validators.required, Validators.pattern('[a-zA-Z]{1,30}') ] ) ]
    });

    // Asignación de los validadores de nuestro controlador padre a nuestro controlador hijo (input)
    this.name = this.contactForm.controls.name;

  }

  ngOnInit(): void {
  }

  public checkName() {

    if ( this.name.status === 'INVALID' ) {
      if ( this.name.value === '' ) {
        this.validName = true;
      } else {
        this.validName = false;
      }

      this.validInput = true;
    } else {
      this.validInput = false;
    }
  }

  public keyUpName() {

    console.log( "KeyUp: " + this.name.value );

  }

  public blurName() {

    console.log( this.name );
  }

  public viewName() {

    if ( !this.validInput ) {
      this.nameInput = this.name.value;
    }

  }

}
